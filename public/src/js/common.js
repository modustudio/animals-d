
// 헤더 고정
$(function(){
	$(window).on("scroll", function(e) {
		var h = 0;
		$("[data-header='true']").each(function(){
			h = h + $(this).height();
		})
		if ($(this).scrollTop() > h) {
			$("#fixedHeader").addClass("js-fixed");
			$("#fixedSidebar").addClass("js-fixed");
		} else {
			$("#fixedHeader").removeClass("js-fixed");
			$("#fixedSidebar").removeClass("js-fixed");
		}
	});
});

// 맨 위로
$(function(){
	$("#goToTop").on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: 0
		}, 300);
	});
});

// Breadcrumb
$(function(){
	var crumbBtn = $("[data-id='crumbMenuBtn']");
	$(crumbBtn).each(function(){
		var _this = $(this);
		_this.attr("tabindex","0");
		_this.on("click keypress",function(e){
			e.stopPropagation();
			if ((e.keyCode == 13)||(e.type == 'click')) {
				if ($(this).next().hasClass("js-active")) {
					$(this).next().removeClass("js-active");
				} else {
					$(crumbBtn).next().removeClass("js-active");
					$(this).next().addClass("js-active");
				}
			}
		});
		_this.next().find("a").last().on("blur",function(e){
			console.log("last");
			_this.next().removeClass("js-active");
		});
	});
	$("#AnimalsWrap").on("click",function(e){
		e.stopPropagation();
		if ($(crumbBtn).next().hasClass("js-active")) $(crumbBtn).next().removeClass("js-active");
	});
});

$(window).ready(function(){
	$('.gnb').on('mouseenter',function(){
		$('.gnb__depth2-list, .gnb__bg').show();
	}).on('mouseleave',function(){
		$('.gnb__depth2-list, .gnb__bg').hide();
		$('.gnb__depth1-link').removeClass('is-active');
	});
	$('.gnb__depth2-list').on('mouseenter',function(){
		$('.gnb__depth1-link').removeClass('is-active');
		$(this).parent().find('.gnb__depth1-link').addClass('is-active');
	});
	$('.gnb__depth1-item').on('mouseenter',function(){
		$('.gnb__depth1-link').removeClass('is-active');
		$(this).find('.gnb__depth1-link').addClass('is-active');
	});

	// 연혁
	$(function(){
		$(".history__year-tab li").click(function(){
			var thisIdx = $(this).index();
			$(this).addClass("on").siblings().removeClass('on');
			$(".history__year-content li").eq(thisIdx).show().siblings().hide();
		});
	});

});



$(window).load(function(){
	// 주요활동-반려동물
	$(function(){
		// $(".imageList__listTitle").dotdotdot({
		// 	after: "img.newTitleIcon, span.viewCntIcon"
		// });
		// $(".newTitle").append('<img class="newTitleIcon" src="../../images/content/listNewIcon.png"/>');
		$('.imageList__description').dotdotdot();
		$('.event__txt').dotdotdot({
			height:42
		});
		var selectBox = $("select").selectBoxIt({autoWidth:false});
	});
})



//tab-v1
$(function() {
	var tab = $(".tab-v1");
	var index = 0;
	var hash = window.location.hash;
	var tt= [];
	$(tab).find("a").each(function(idx){
		var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
		tt.push(t);

		if (hash) {
			index = /\d+/.exec(hash)[0];
			index = (parseInt(index) || 1) ;
			sele($(tab).find("a[href="+hash+"]"));
			for (var i in tt) {
				$("#"+tt[i]).removeClass("is-active");
			}
			$(hash).addClass("is-active");
		}

		$(this).on("click", function(e){
			if (this.href.match(/#([^ ]*)/g)) {
				e.preventDefault();
				if (!$(this).parent().hasClass("is-active")) window.location.hash = /\d+/.exec($(this).attr("href")).input;
				sele($(this));
				for (var i in tt) {
					$("#"+tt[i]).removeClass("is-active");
				}
				$("#"+t).addClass("is-active");
			}
		});



	})

	$(tab).find("[class$=list]").on("click", function(){
		//console.log($('body').attr('data-mobile'));
		if ($('body').attr('data-mobile') == 'true'){
			if ($(this).hasClass("js-open-m")) {
				$(this).removeClass("js-open-m");
			} else {
				$(this).addClass("js-open-m");
			}
		}
	});

	function sele(el) {
		$(el).parent().addClass("is-active").siblings().removeClass("is-active");
	}

	$(window).resize(function(){
		if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
	});
});



$(document).ready(function () {
	$(document).on("scroll", onScroll);

	$('a[href^="#"]').on('click', function (e) {
		e.preventDefault();
		$(document).off("scroll");

		$('a').each(function () {
			$(this).removeClass('active');
		})
		$(this).addClass('active');

		var target = this.hash;
		var hH = $('.breadcrumb').height();
		$target = $(target);
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top-hH
		}, 500, 'swing', function () {
			window.location.hash = target;
			$(document).on("scroll", onScroll);
		});
	});
});

function onScroll(event){
	var scrollPosition = $(document).scrollTop();
	$('nav a').each(function () {
		var currentLink = $(this);
		var refElement = $(currentLink.attr("href"));
		if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
			$('nav ul li a').removeClass("active");
			currentLink.addClass("active");
		}
		else{
			currentLink.removeClass("active");
		}
	});
}


//input:file
$(document).ready(function() {
	var fileTarget = $('.upload-hidden');

	fileTarget.on('change', function() {
		if (window.FileReader) {
			// 파일명 추출
			var filename = $(this)[0].files[0].name;
		} else {
			// Old IE 파일명 추출
			var filename = $(this).val().split('/').pop().split('\\').pop();
		};

		$(this).parent().siblings('.upload-name').val(filename);
	});

	//preview image
	var imgTarget = $('.upload-hidden');

	imgTarget.on('change', function() {
		var parent = $(this).parent().parent();
		parent.children('.upload-display').remove().end().removeClass('upload-set-v1_thumb');

		if (window.FileReader) {
			//image 파일만
			if (!$(this)[0].files[0].type.match(/image\//)) return;

			var reader = new FileReader();
			reader.onload = function(e) {
				var src = e.target.result;
				parent.prepend('<span class="upload-display"><img src="' + src + '" class="upload-thumb"></span>').addClass('upload-set-v1_thumb');
			}
			reader.readAsDataURL($(this)[0].files[0]);
		} else {
			//$(this)[0].select();
			//$(this)[0].blur();
			//var imgSrc = document.selection.createRange().text;
			//parent.prepend('<span class="upload-display"><img class="upload-thumb"></span>').addClass('upload-set-v1_thumb');

			//var img = $(this).parent().siblings('.upload-display').find('img');
			//img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\"" + imgSrc + "\")";
		}
	});

});

$(window).load(function(){
	var main3Slider = $('.main-content3__list').bxSlider({
		mode: 'horizontal',
		pager: false,
		controls: false,
		onSlideAfter: function(){
		 $('.main-content3__number').text((main3Slider.getCurrentSlide()+1)+'/'+main3Slider.getSlideCount());
		}
	});
	//초기넘버셋팅
	var main3Slide = $('.main-content3__list li').length-2;
	$('.main-content3__number').text((1+'/'+main3Slide));
	$( '.js-main3_prev' ).on( 'click', function () {
		main3Slider.goToPrevSlide();
		return false;

	});
	$( '.js-main3_next' ).on( 'click', function () {
		main3Slider.goToNextSlide();
		return false;
	});

	var visualList = $('.imgSlide__list').bxSlider({
		 mode: 'horizontal',
		 controls: false,
		 pager: false
	});
	$( '.imgSlide__btn-prev' ).on( 'click', function () {
		visualList.goToPrevSlide();
		return false;
	});
	$( '.imgSlide__btn-next' ).on( 'click', function () {
		visualList.goToNextSlide();
		return false;
	});

});
