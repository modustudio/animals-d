$(window).load(function(){
	var visualList = $('.visual__list').bxSlider({
		 mode: 'horizontal',
		 controls: false
	});
	$( '.js-visual_prev' ).on( 'click', function () {
		visualList.goToPrevSlide();
		return false;
	});
	$( '.js-visual_next' ).on( 'click', function () {
		visualList.goToNextSlide();
		return false;
	});
	var midSlider = $('.main-content7__list').bxSlider({
		 mode: 'horizontal',
		 auto:true,
		 pager: true,
		 controls: true,
		 autoControls:true,
		 pagerSelector:'.main-content7__paging',
		 autoControlsSelector:'.main-content7__play',
		 autoControlsCombine:true
	});
	$( '.js-mid_prev' ).on( 'click', function () {
		midSlider.goToPrevSlide();
		return false;
	});


	$( '.js-mid_next' ).on( 'click', function () {
		midSlider.goToNextSlide();
		return false;
	});
	var botSlider = $('.main-content8__list').bxSlider({
		 mode: 'horizontal',
		 pager: false,
		 controls: false,
		 onSlideAfter: function(){
		  $('.main-content8__number').text((botSlider.getCurrentSlide()+1)+'/'+botSlider.getSlideCount());
		 }
	});
	//초기넘버셋팅
	var botSlide = $('.main-content8__list li').length-2;
	$('.main-content8__number').text((1+'/'+botSlide));

	$( '#prevBtn' ).on( 'click', function () {
		botSlider.goToPrevSlide();
		return false;
	});
	$( '#nextBtn' ).on( 'click', function () {
		botSlider.goToNextSlide();
		return false;
	});


	tab3();
	function tab3(){
		$('.js-toggle').on('mouseenter',function(){
			var num = $(this).index();
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$('.js-body').eq(num).show().siblings().hide();
		});
	}
	tab2();
	$('.main-content1__board').eq(0).show();
	function tab2(){
		$('.js-tab').on('click',function(e){
			e.preventDefault();
			var data = $(this).parent('h3').next();
			var dataAll = $('.js-tab').parent('h3').next();
			dataAll.hide();
			data.show();
		});
	}
	$('.js-dot').dotdotdot({
		wrap : 'word',
		height: 40
		// height: function(){
		// 	$('.js-dot').attr('data-item')
		// }
	});
	$('.main-content3__body-item').eq(0).show();
});
