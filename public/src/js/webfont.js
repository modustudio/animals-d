WebFont.load({
	custom: {
		families: ['Nanum Gothic'],
		urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
	},
	google: {
		families: ['Open Sans']
	},
	active: function() {
		sessionStorage.fonts = true;
	}
});
